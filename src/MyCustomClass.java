import java.util.ArrayList;

public class MyCustomClass<T> {
    private int size = 0;
    ArrayList<T> arrayList = new ArrayList<>();

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public T listItem(int i) {
        return arrayList.get(i);
    }

    public void addItem(T item) {
        if (arrayList.contains(item)) {
            throw new DuplicateException("You duplicated an item: " + item);
        } else if (item == null) {
            throw new NumberFormatException("You can't add null item!");
        } else {
            arrayList.add(item);
            size++;
        }
    }
}

