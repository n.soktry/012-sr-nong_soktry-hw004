public class Main {
    public static void main(String[] args) {
        MyCustomClass<Integer> customClass = new MyCustomClass<>();
        
        try {
            for (int i = 0; i < 10; i++) {
                customClass.addItem(i);
            }
            customClass.addItem(7);
        } catch (DuplicateException | NumberFormatException e) {
            System.out.println(e);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nList All Items Added:");

        for (int i = 0; i < customClass.getSize(); i++) {
            System.out.println("Item: " + customClass.listItem(i));
        }
    }
}

